let studentList = [];

function addStudent(addStudentName){
    studentList.push(addStudentName);
    console.log(`${addStudentName} was added to the Student List.`);
}

function countStudents(){
    console.log(`There are a total of ${studentList.length} enrolled.`);
}

function printStudent(){
    studentList.sort();
    console.log(studentList);

    studentList.forEach(function(student){
        console.log(student);
    });
}

function findStudent(keyword){
    let match = studentList.filter(function(studentList){
        return studentList.toLowerCase().includes(keyword.toLowerCase());
    })
    if(match.length == 1){
        console.log(`${match} is an Enrollee.`);

    } else {
        console.log(`No Student found with the name ${match}`);
    }
    }